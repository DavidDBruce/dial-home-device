﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Unity.Mathematics;
using UnityEngine;

namespace Scenes
{
    public class GateBehavior : MonoBehaviour
    {
        private const char BadGlyph = '~';
        private const char DialButton = ' ';

        private Stargate _stargate;

        // Start is called before the first frame update
        private void Start()
        {
            _stargate = new Stargate();
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.anyKey && (AddressIsNotFull() || DialButtonIsNotPressed() || GateIsActive()))
                UpdateAddress();

            if (GateIsNotInMotion())
                if (AddressIsNotComplete() && ThereAreUndialedChevrons())
                    DialNextChevron();
                else if (DialButtonIsPressed() && GateIsNotActive())
                    AttemptActivation();
        }

        private void DialNextChevron()
        {
            _stargate.DialNext();
        }

        private void AttemptActivation()
        {
            _stargate.AttemptActivation();
        }

        private bool GateIsNotInMotion()
        {
            return !_stargate.GateIsInMotion();
        }

        private bool DialButtonIsPressed()
        {
            return _stargate.IsDialButtonPressed();
        }

        private bool DialButtonIsNotPressed()
        {
            return !_stargate.IsDialButtonPressed();
        }

        private bool GateIsActive()
        {
            return _stargate.GateIsActive;
        }

        private bool GateIsNotActive()
        {
            return !_stargate.GateIsActive;
        }

        private void UpdateAddress()
        {
            var glyphPressed = RetrieveValidPressedGlyphOrTilda();
            if (glyphIsDialButton(glyphPressed))
            {
                if (GateIsActive())
                    _stargate.CloseGate();
                else
                    _stargate.DialButtonPressed();
            }
            else if (AddressIsNotFull() &&
                     PressedGlyphIsNotAlreadyInAddress(glyphPressed) &&
                     PressedGlyphIsValidGlyph(glyphPressed))
            {
                _stargate.AddChevron(glyphPressed);
            }
        }

        private bool AddressIsNotFull()
        {
            return !_stargate.DialingSequence.IsFull();
        }

        private bool AddressIsNotComplete()
        {
            return !_stargate.AddressIsComplete();
        }

        private bool ThereAreUndialedChevrons()
        {
            return _stargate.DialingSequence.ThereAreUndialedChevrons();
        }

        private bool PressedGlyphIsNotAlreadyInAddress(char pressedGlyph)
        {
            return _stargate.DialingSequence.GlyphIsNotAlreadyInAddress(pressedGlyph);
        }

        private bool PressedGlyphIsValidGlyph(char glyphPressed)
        {
            return BadGlyph != glyphPressed;
        }

        private bool glyphIsDialButton(char glyphPressed)
        {
            return DialButton == glyphPressed;
        }

        private char RetrieveValidPressedGlyphOrTilda()
        {
            foreach (var symbol in Stargate.GlyphSymbols)
                if (Input.GetKeyDown(symbol.ToString()))
                    return symbol;

            if (Input.GetKeyDown("space"))
                return ' ';

            return BadGlyph;
        }
    }

    internal class Stargate
    {
        public static readonly char[] GlyphSymbols =
        {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '.'
        };

        public static readonly Dictionary<char, Chevron> Chevrons = PopulateChevrons();

        private readonly GameObject _chevronLocker;


        public Stargate()
        {
            StargateRing = GameObject.Find("Glyph Ring");
            _chevronLocker = GameObject.Find("C7-Inside");
            GateIsActive = false;
            DialingSequence = new DialingSequence();
            StargateRing.transform.Translate(0, 0, 0);
            _chevronLocker.transform.Translate(0,0,0);
        }

        private GameObject StargateRing { get; }

        public bool GateIsActive { get; private set; }

        public DialingSequence DialingSequence { get; }

        private static Dictionary<char, Chevron> PopulateChevrons()
        {
            var allChevrons = new Dictionary<char, Chevron>();
            for (var i = 0; i < GlyphSymbols.Length; i++)
                allChevrons.Add(GlyphSymbols[i], Chevron.Of(DialLocation.Of(i + 1), GlyphSymbols[i]));
            return allChevrons;
        }

        public bool IsDialButtonPressed()
        {
            return DialingSequence.DialButtonPressed;
        }

        public void AddChevron(char glyphPressed)
        {
            DialingSequence.AddChevron(Chevrons[glyphPressed]);
        }

        public bool GateIsInMotion()
        {
            var gateIsInMotion = DOTween.TotalPlayingTweens() != 0;
            Debug.Log("GateIsInMotion: " + gateIsInMotion);
            return gateIsInMotion;
        }

        public void DialNext()
        {
            var currentChevron = DialingSequence.NextChevron();
            RotateInnerRingTo(currentChevron);
            DialingSequence.Dial(currentChevron);
        }

        public bool AddressIsComplete()
        {
            return DialingSequence.AddressIsComplete();
        }

        private void RotateInnerRingTo(Chevron chevron)
        {
            DOTween.Sequence()
                .Append(SpinGate(chevron))
                .Append(LockChevron())
                .Append(UnlockChevron())
                .Play();
        }

        private Tween SpinGate(Chevron chevron)
        {
            var chevronAngle = retrieveAngleOfChevron(chevron);
            var chevronDistance = DetermineChevronDistance(chevron);
            return StargateRing.transform.DORotate(chevronAngle, chevronDistance).SetEase(Ease.InOutSine);
        }

        private Tween LockChevron()
        {
             return _chevronLocker.transform.DOMoveZ(-0.32f, 0.5f).SetEase(Ease.InOutSine);
        }

        private Tween UnlockChevron()
        {
            return _chevronLocker.transform.DOMoveZ(0f, 0.5f).SetEase(Ease.InOutSine);
        }

        private Vector3 retrieveAngleOfChevron(Chevron chevron)
        {
            return new Vector3(chevron.DialLocation.Angle() + 150, 90, 90);
        }

        private float DetermineChevronDistance(Chevron chevron)
        {
            var previousChevronNumber = DeterminePreviousChevronNumber();
            var currentChevronNumber = chevron.DialLocation.ChevronNumber;
            var oneWayDistance = math.abs(previousChevronNumber - currentChevronNumber);
            float chevronDistance;
            if (oneWayDistance <= 20)
                chevronDistance = oneWayDistance;
            else
                chevronDistance = 39 - oneWayDistance;
            // Debug.Log(
            //     "Distance is: " + chevronDistance + " for " + previousChevronNumber + " - " + currentChevronNumber);
            return chevronDistance / 4f + 2f;
        }

        private int DeterminePreviousChevronNumber()
        {
            if (DialingSequence.DialedChevrons.Count != 0)
                return DialingSequence.DialedChevrons.Last().DialLocation.ChevronNumber;
            return 0;
        }

        public void AttemptActivation()
        {
            if (DialingSequence.IsFull())
            {
                GateIsActive = true;
                //TODO animate wormhole opening.
                Debug.Log("Gate Activating");
                Debug.Log(
                    $"{RetrieveChevronStringAtOrEmpty(0)}{RetrieveChevronStringAtOrEmpty(1)}{RetrieveChevronStringAtOrEmpty(2)}-{RetrieveChevronStringAtOrEmpty(3)}{RetrieveChevronStringAtOrEmpty(4)}{RetrieveChevronStringAtOrEmpty(5)} {RetrieveChevronStringAtOrEmpty(6)}");
            }
            else
            {
                DialingSequence.Reset();
                //TODO animate gate dial failure.
                Debug.Log("Activation Failure");
            }
        }

        private string RetrieveChevronStringAtOrEmpty(int index)
        {
            try
            {
                return DialingSequence.DialedChevrons.ElementAt(index).GlyphSymbol.ToString().ToUpper();
            }
            catch (Exception)
            {
                return "";
            }
        }

        public void CloseGate()
        {
            GateIsActive = false;
            DialingSequence.Reset();
            //TODO Animate gate closing. 
            Debug.Log("Gate Closing");
        }

        public void DialButtonPressed()
        {
            DialingSequence.DialButtonPressed = true;
        }
    }

    internal class DialingSequence
    {
        public DialingSequence()
        {
            UndialedChevrons = new List<Chevron>();
            DialedChevrons = new List<Chevron>();
            DialButtonPressed = false;
        }

        private List<Chevron> UndialedChevrons { get; }
        public List<Chevron> DialedChevrons { get; }

        public bool DialButtonPressed { get; set; }

        public void AddChevron(Chevron chevron)
        {
            if (DialButtonPressed == false) UndialedChevrons.Add(chevron);
        }

        public bool ThereAreUndialedChevrons()
        {
            return UndialedChevrons.Count > 0;
        }

        public Chevron NextChevron()
        {
            if (ThereAreUndialedChevrons()) return UndialedChevrons.ElementAt(0);

            return null;
        }

        public void Dial(Chevron chevron)
        {
            DialedChevrons.Add(chevron);
            UndialedChevrons.Remove(chevron);
        }

        public bool IsFull()
        {
            return DialedChevrons.Count + UndialedChevrons.Count >= 7;
        }

        public void Reset()
        {
            UndialedChevrons.Clear();
            DialedChevrons.Clear();
            DialButtonPressed = false;
        }

        public bool GlyphIsNotAlreadyInAddress(char pressedGlyph)
        {
            foreach (var dialedChevron in DialedChevrons)
                if (dialedChevron.GlyphSymbol == pressedGlyph)
                    return false;

            foreach (var undialedChevron in UndialedChevrons)
                if (undialedChevron.GlyphSymbol == pressedGlyph)
                    return false;

            return true;
        }

        public bool AddressIsComplete()
        {
            return DialedChevrons.Count == 7;
        }
    }

    internal class Chevron
    {
        private Chevron(DialLocation dialLocation, char glyphSymbol)
        {
            DialLocation = dialLocation;
            GlyphSymbol = glyphSymbol;
        }

        public DialLocation DialLocation { get; }
        public char GlyphSymbol { get; }

        public static Chevron Of(DialLocation dialLocation, char glyphSymbol)
        {
            return new Chevron(dialLocation, glyphSymbol);
        }
    }

    internal class DialLocation
    {
        private const float ThreeSixty = 360f;
        private const float TotalChevrons = 39;

        private DialLocation(int chevronNumber)
        {
            ChevronNumber = chevronNumber;
        }

        public int ChevronNumber { get; }

        public static DialLocation Of(int chevronNumber)
        {
            return new DialLocation(chevronNumber);
        }

        public float Angle()
        {
            return ChevronNumber * (ThreeSixty / TotalChevrons);
        }
    }
}